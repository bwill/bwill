# About Me :wave:

Hi! I'm Brian and I am a Backend Engineer on the :lock: Govern: Threat Insights team
at GitLab. I work on [Vulnerability Management](https://about.gitlab.com/direction/govern/threat_insights/vulnerability_management/),
and [Dependency Management](https://about.gitlab.com/direction/govern/threat_insights/dependency_management/)
(in particular, lots of things involving [SBoMs](https://cyclonedx.org/)).
I'm also a frequent contributor to the authentication, authorization, and cryptography
aspects of GitLab. I have a strong knowledge of application security thanks to spending
5 years as an Application Security Engineer, and a passion for applied cryptography.
I am also a [merge request coach](https://about.gitlab.com/handbook/marketing/community-relations/code-contributor-program/resources/merge-request-coach-lifecycle.html),
so I am frequently helping out community contributors and reviewing their merge requests.

Some cryptography things I have worked on:

- [tlstools](https://github.com/brcrwilliams/tlstools) - Basically `openssl s_client`, but better
- [ssh_sig](https://gitlab.com/bwill/ssh_sig) - Pure ruby implementation of SSH signature verification

## Availablility

I live in Austin, Texas, USA :flag_us: (UTC -5 or -6, depending on the season) and I'm usually
online around 12:00 UTC to 21:00 UTC Monday through Friday. This schedule ignores US time zones.
For example, when daylight savings time ends in the fall, and clocks in the US move backwards
one hour, my start time also moves backwards from 7 am to 6 am. Generally, I try to keep
my routine aligned with the daylight hours rather than the clock. I prefer to wake up
around 30 minutes before sunrise, grab a coffee and some breakfast, and spend some time
looking out the window. :sunrise: :coffee:

**12:00 UTC - 15:00 UTC are designated as code review hours.**
To avoid excess context switching, I try to complete all my code reviews during this
time. I won't review your MR outside these hours unless it's something really small
(ex: feature flag removal, dropping a single DB column, localizing a string),
or I've already reviewed at your MR at least once.

When my GitLab status is Busy :red_circle:, it means that I'm at capacity and
cannot take on new code reviews. While I am using the Busy status, you can
still assign me MRs for review if:

- I already reviewed your MR and you need a re-review
- Your MR is changing a feature belonging to the Threat Insights group
- Everyone else in the reviewer pool for the project is also busy or unavailable

## Communication

I am Deaf, which makes spoken communication and meetings very inefficient for me.
As such, I value written and asynchronous communication quite highly.

- I prefer to communicate using issues and merge requests where possible.
- I will always value a well-constructed written message over a request for a "quick chat".
  I absolutely _do not_ mind if you write a 1-3 page long comment. I will read it, and I greatly
  appreciate your effort and detail.
- If you are hosting a Zoom meeting with me, please [enable captions and live transcription](https://about.gitlab.com/handbook/tools-and-tips/zoom/#enable-captions-and-live-transcription)
- When asking questions in Slack, please provide as much detail as you can up front.
  Given GitLab's asynchronous nature, it could be several hours before you receive a response,
  so back-and-forth can be costly. When asking technical quesitons, it may be helpful to
  remember to state or ask some of these things:

  - What are you trying to accomplish? (Why are you doing the thing you're doing?)
  - What have you tried so far?
  - Examine your question. [Are you asking about your problem or your attempted solution?](https://xyproblem.info/)
  - State any theories or assumptions you have about what you think is happening or
    what you think you should be doing to solve the issue.
  - Share logs, config files, CI YAML, or code if you have any.
  - Link to a CI job or a merge request if you can.
  - Copy-paste rather than taking screenshots of text.
